package com.example.imcgroup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImcgroupApplication {

	public static void main(String[] args) {
		System.out.print("Test Class");
		SpringApplication.run(ImcgroupApplication.class, args);
	}
}
